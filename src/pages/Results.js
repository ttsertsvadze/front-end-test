import "./Results.css";
import { Link } from "react-router-dom";
import chevron from "../assets/Chevron.svg";

export default function Results() {
  return (
    <div className="results">
      <h1 id="results--header">Results</h1>
      <p id="results--paragraph">Order basket redesign</p>
      <div className="back">
        <img
          src={chevron}
          alt="design of a chevron oriented to the left"
          id="back--image"
        ></img>
        <button id="back--button">
          <Link to={"/"}>
            <span id="back--button--text">Back</span>
          </Link>
        </button>
      </div>
    </div>
  );
}
