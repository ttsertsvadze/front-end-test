import { memo } from "react";
import "./Dashboard.css";
import chevron from "../assets/Chevron.svg";
import search from "../assets/Search.svg";
import { useState } from "react";
import { testsData } from "../testsData";
import Card from "../elements/Card";

function Dashboard() {
  const [sortByName, setSortByName] = useState(true);
  const [sortByType, setSortByType] = useState(true);
  const [sortBySite, setSortBySite] = useState(true);
  const [sortByStatus, setSortByStatus] = useState(true);
  const [query, setQuery] = useState();
  const [result, setResult] = useState(testsData);
  const status = {
    ONLINE: 1,
    PAUSED: 2,
    STOPPED: 3,
    DRAFT: 4,
  };

  function handleChange(event) {
    const results = testsData.filter((test) => {
      if (event.target.value === "") {
        return testsData;
      } else {
        return test.name
          .toLowerCase()
          .includes(event.target.value.toLowerCase());
      }
    });

    setQuery(results);
  }

  function handleSearch() {
    setResult(query);
  }

  function handleReset() {
    setResult(testsData);
  }

  function sortFuncName(event) {
    let sortedResult;

    if (sortByName === true) {
      sortedResult = [...result].sort((a, b) =>
        a[event.target.name].localeCompare(b[event.target.name])
      );
    } else if (sortByName === false) {
      sortedResult = [...result].sort((a, b) =>
        b[event.target.name].localeCompare(a[event.target.name])
      );
    }

    setResult(sortedResult);
    setSortByName((prevSortByName) => !prevSortByName);
    setSortByType(true);
    setSortByStatus(true);
    setSortBySite(true);
  }

  function sortFuncType(event) {
    let sortedResult;

    if (sortByType === true) {
      sortedResult = [...result].sort((a, b) =>
        a[event.target.name].localeCompare(b[event.target.name])
      );
    } else if (sortByType === false) {
      sortedResult = [...result].sort((a, b) =>
        b[event.target.name].localeCompare(a[event.target.name])
      );
    }

    setResult(sortedResult);
    setSortByType((prevSortByType) => !prevSortByType);
    setSortByName(true);
    setSortByStatus(true);
    setSortBySite(true);
  }

  function sortFuncSite(event) {
    let sortedResult;

    if (sortBySite === true) {
      sortedResult = [...result].sort((a, b) =>
        a[event.target.name].localeCompare(b[event.target.name])
      );
    } else if (sortBySite === false) {
      sortedResult = [...result].sort((a, b) =>
        b[event.target.name].localeCompare(a[event.target.name])
      );
    }

    setResult(sortedResult);
    setSortBySite((prevSortBySite) => !prevSortBySite);
    setSortByName(true);
    setSortByType(true);
    setSortByStatus(true);
  }

  function sortFuncStatus() {
    let sortedResult;

    if (sortByStatus === true) {
      sortedResult = [...result].sort(
        (a, b) => status[a.status] - status[b.status]
      );
    } else if (sortByStatus === false) {
      sortedResult = [...result].sort(
        (a, b) => status[b.status] - status[a.status]
      );
    }

    setResult(sortedResult);
    setSortByStatus((prevSortByStatus) => !prevSortByStatus);
    setSortByName(true);
    setSortByType(true);
    setSortBySite(true);
  }

  return (
    <>
      <div className="container">
        <h1 className="header">Dashboard</h1>
        <div className="search_bar">
          <img
            src={search}
            alt="search bar logo"
            id="search_bar--logo"
            onClick={handleSearch}
          />
          <input
            placeholder="What test are you looking for?"
            id="search_bar--input"
            onChange={handleChange}
          />
          <p id="search_bar--tests">
            {result.length} test{result.length > 1 && "s"}
          </p>
        </div>
        <div className="dashboard">
          {result.length !== 0 && (
            <div className="categories">
              <button id="categories--name" name="name" onClick={sortFuncName}>
                NAME
              </button>
              <div className="categories--type">
                <button
                  id="categories--type_text"
                  name="type"
                  onClick={sortFuncType}
                >
                  TYPE
                </button>
                <img
                  src={chevron}
                  alt="symbol of a chevron"
                  id="categories--type_image"
                />
              </div>
              <button id="categories--status" onClick={sortFuncStatus}>
                STATUS
              </button>
              <button id="categories--site" name="site" onClick={sortFuncSite}>
                SITE
              </button>
            </div>
          )}
        </div>
        <div className="cards">
          {result.length > 0 ? (
            result.map((test) => <Card key={test.id} test={test} />)
          ) : (
            <div className="no_results">
              <p id="no_results--text">
                Your search did not match any results.
              </p>
              <button id="no_results--button" onClick={handleReset}>
                Reset
              </button>
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default memo(Dashboard);
