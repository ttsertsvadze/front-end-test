import data from "./db-1690799964806.json";

export const testsData = data.tests
  .map((test) => {
    for (let i = 0; i < data.sites.length; i++) {
      if (test.siteId === data.sites[i].id) {
        return { ...test, site: data.sites[i].url.split("//")[1] };
      }
    }
  })
  .map((test) => {
    if (test.site === "market.company.com") {
      return { ...test, borderStyle: { backgroundColor: "#E14165" } };
    } else if (test.site === "www.delivery.company.com") {
      return { ...test, borderStyle: { backgroundColor: "#C2C2FF" } };
    } else {
      return { ...test, borderStyle: { backgroundColor: "#8686FF" } };
    }
  });
