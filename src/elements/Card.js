import "./Card.css";
import { Link } from "react-router-dom";

export default function Card({ test }) {
  let statusColor;
  if (test.status === "Online") {
    statusColor = { color: "#1BDA9D" };
  } else if (test.status === "PAUSED") {
    statusColor = { color: "#FF8346" };
  } else if (test.status === "DRAFT") {
    statusColor = { color: "#5C5C5C" };
  } else if (test.status === "STOPPED") {
    statusColor = { color: "#FE4848" };
  }

  return (
    <div className="card">
      <div className="card--border" style={test.borderStyle}></div>
      <div className="card--content">
        <h1 id="card--name">{test.name}</h1>
        <h4 id="card--type">
          {test.type === "MVT"
            ? test.type
            : test.type.charAt(0) + test.type.slice(1).toLowerCase()}
        </h4>
        <h4 id="card--status" style={statusColor}>
          {test.status.charAt(0) + test.status.slice(1).toLowerCase()}
        </h4>
        <h4 id="card--site">{test.site}</h4>
        <button
          id="card--button"
          style={
            test.status === "DRAFT"
              ? { backgroundColor: "#7D7D7D" }
              : { backgroundColor: "#2ee5ac" }
          }
        >
          {test.status === "DRAFT" ? (
            <Link to={`/finalize/${test.id}`}>
              <span className="button_text">Finalize</span>
            </Link>
          ) : (
            <Link to={`/results/${test.id}`}>
              <span className="button_text">Results</span>
            </Link>
          )}
        </button>
      </div>
    </div>
  );
}
